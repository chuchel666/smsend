import requests
from random import randint
import telebot
from bs4 import BeautifulSoup as bs
from config import token

root = 'https://smsend.ru/sent-sms.php'
headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh;' \
            'Intel Mac OS X 10.9; rv:45.0) ' \
            'Gecko/20100101 Firefox/45.0'
        }

def page(url):
    rawpage = requests.get(url, headers=headers)
    if rawpage.ok:
        return bs(rawpage.text, 'html.parser')

def endpage() -> int:
    links = page(root).find_all('a')
    if 'Конец' in links[6].text:
        end = links[6]['href'].split('=')[-1]
        return int(end)

def quotes(page) -> list:
    return page.find_all('div', class_='center-col__content-send_mess')

def quote(quotes, n) -> list:
    print(quotes)
    print(len(quotes))
    print(n)
    return quotes[n].text.strip().split('\n')

if __name__ == '__main__':
    randsmslist = quotes(
                        page(
                            root + f'?PAGEN_1={randint(1, endpage())}'
                            )
                        )
    print(quote(randsmslist, randint(0, len(randsmslist))))